#version 450

in vec3 a_Position;	//vs input 선언, float 3개, in -> vs input -> Attrib
in vec3 a_Vel;		// vs input 선언, float 3개, 속도
in float a_StartTime; // vs input 선언, float 1개, 생성시간
in float a_LifeTime;	//vs input 초 생명
in float a_Period;
in float a_Amp;
in float a_Value;

in vec4 a_Color; //vs input 선언, float 4개, in -> vs input -> Attrib

out vec4 v_Color;	//vertex shader output

//uniform float u_Scale;

//uniform float u_Angle;

uniform float u_Time;

vec3 c_Gravity = vec3(0, -0.25, 0);
bool c_bLoop = true;
float c_PI = 3.141592;

void main()
{	
	float newTime = u_Time - a_StartTime; // 생성 이후 지난 시간
	vec2 initParametricPos = vec2(0.7 * sin(a_Value * 2.0 * c_PI), 0.7 * cos(a_Value * 2.0 * c_PI));
	vec4 newPos = vec4(a_Position.xy + initParametricPos, 0, 1);

	float period = a_Period;	//주기
	float amp = a_Amp;	//폭
	float alpha = 0.0;
		
	if(newTime > 0)
	{
	
		float tempTime = newTime;
		if(c_bLoop)
		{
			tempTime = fract(tempTime / a_LifeTime ) * a_LifeTime;
			alpha = fract(tempTime / a_LifeTime );
		}
		newPos.xyz = newPos.xyz + a_Vel.xyz * tempTime + 0.5 * c_Gravity * tempTime * tempTime;

		vec2 rotVel = vec2(-(a_Vel.y + c_Gravity.y * tempTime), (a_Vel.x + c_Gravity.x * tempTime));
		rotVel = normalize(rotVel);
		newPos.xy = newPos.xy + tempTime * rotVel * amp * sin(2.0 * period * c_PI * tempTime);
	}
	else
	{
	newPos = vec4(-100000,-100000,0,1);
	}

	gl_Position = newPos;

	v_Color = vec4(a_Color.rgb, 1.0 - alpha);
}

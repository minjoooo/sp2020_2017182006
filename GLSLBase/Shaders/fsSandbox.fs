#version 450

layout(location=0) out vec4 FragColor;

in vec2 v_TexCoord;

float c_PI = 3.141592;

uniform float u_Time = 0.0;

//uniform vec2 u_InputPoints[10];

void main()
{
	vec2 inputPoint = vec2(0.75, 0.0);
	float inputPointSize = 0.05;

	int isCircleLineRegion = 0;

	float distance1 = distance(v_TexCoord, vec2(0.5,0.5));
	float comparDis = fract(u_Time);

	if (distance1 < comparDis && distance1 > comparDis - 0.01)
	{
		isCircleLineRegion = 1;
		FragColor = vec4(0.5);
	}
	else
	{
		FragColor = vec4(0);
	
	}

	if(isCircleLineRegion > 0)
	{
		float distance0 = distance(v_TexCoord, inputPoint);
		if( distance0 < inputPointSize)
		{
			FragColor += vec4(1.0);
		}
	
	}
}

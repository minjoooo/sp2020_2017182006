#include "stdafx.h"
#include "Renderer.h"
#include "LoadPng.h"
#include <Windows.h>
#include <cstdlib>
#include <cassert>

Renderer::Renderer(int windowSizeX, int windowSizeY)
{
	Initialize(windowSizeX, windowSizeY);
}


Renderer::~Renderer()
{
}

void Renderer::Initialize(int windowSizeX, int windowSizeY)
{
	//Set window size
	m_WindowSizeX = windowSizeX;
	m_WindowSizeY = windowSizeY;

	//Load shaders
	m_SolidRectShader = CompileShaders("./Shaders/SolidRect.vs", "./Shaders/SolidRect.fs");
	m_FSSandboxShader = CompileShaders( "./Shaders/vsSandbox.vs", "./Shaders/fsSandbox.fs" );
	
	//Create VBOs
	CreateVertexBufferObjects();
}

void Renderer::CreateVertexBufferObjects()
{
	float rect[]
		=
	{
		-0.5, -0.5, 0.f, -0.5, 0.5, 0.f, 0.5, 0.5, 0.f, //Triangle1
		-0.5, -0.5, 0.f,  0.5, 0.5, 0.f, 0.5, -0.5, 0.f, //Triangle2
	};

	glGenBuffers(1, &m_VBORect);
	glBindBuffer(GL_ARRAY_BUFFER, m_VBORect);
	glBufferData(GL_ARRAY_BUFFER, sizeof(rect), rect, GL_STATIC_DRAW);

	float rectFSSandbox[]
		=
	{
		-0.5, -0.5, 0.f, -0.5, 0.5, 0.f, 0.5, 0.5, 0.f, //Triangle1
		-0.5, -0.5, 0.f,  0.5, 0.5, 0.f, 0.5, -0.5, 0.f, //Triangle2
	};

	glGenBuffers( 1, &m_VBOFSSandbox );
	glBindBuffer( GL_ARRAY_BUFFER, m_VBOFSSandbox );
	glBufferData( GL_ARRAY_BUFFER, sizeof( rectFSSandbox ), rectFSSandbox, GL_STATIC_DRAW );

	float vertices[]
		=
	{
		0.f,0.f,0.f, //v0
		1.f,0.f,0.f, //v1
		1.f,1.f,0.f //v2
	};// CPU memory에 있음 -> GPU memory로 옮겨야 함 (VBO필요)

	glGenBuffers( 1, &m_VBOTest ); //just generate ID
	glBindBuffer( GL_ARRAY_BUFFER, m_VBOTest ); //용도 부여
	glBufferData( GL_ARRAY_BUFFER, sizeof( vertices ), vertices, GL_STATIC_DRAW ); //CPU -> GPU 복사

	float color[]
		=
	{
		1, 0, 0, 1, //v0
		0, 1, 0, 1, //v1
		0, 0, 1, 1 //v2
	};// CPU memory에 있음 -> GPU memory로 옮겨야 함 (VBO필요)

	glGenBuffers( 1, &m_VBOTestColor ); //just generate ID
	glBindBuffer( GL_ARRAY_BUFFER, m_VBOTestColor ); //용도 부여
	glBufferData( GL_ARRAY_BUFFER, sizeof( color ), color, GL_STATIC_DRAW ); //CPU -> GPU 복사

	float particleSize = 0.01;
	float singleParticleVertices[]	//삼각형 두개 -> 사각형 (vertex => 6개)
		=
	{
		-1.f * particleSize,-1.f * particleSize,0.f * particleSize, //v0
		1.f * particleSize,1.f * particleSize,0.f * particleSize, //v1
		-1.f * particleSize,1.f * particleSize,0.f * particleSize, //v2
		-1.f * particleSize,-1.f * particleSize,0.f * particleSize, //v0
		1.f * particleSize,-1.f * particleSize,0.f * particleSize, //v1
		1.f * particleSize,1.f * particleSize,0.f * particleSize//v2
	};// CPU memory에 있음 -> GPU memory로 옮겨야 함 (VBO필요)

	glGenBuffers( 1, &m_VBOSingleParticle ); //just generate ID
	glBindBuffer( GL_ARRAY_BUFFER, m_VBOSingleParticle ); //용도 부여
	glBufferData( GL_ARRAY_BUFFER, sizeof( singleParticleVertices ), singleParticleVertices, GL_STATIC_DRAW ); //CPU -> GPU 복사


	float sign[]
		=
	{
		-0.8f,0.3f,0.f, -0.8f,-0.3f,0.f,
		-0.5f,0.3f,0.f, -0.8f,0.0f,0.f,
		-0.5f,-0.3f,0.f, -0.8f,0.0f,0.f,

		-0.2f,0.3f,0.f, -0.2f,-0.3f,0.f,
		-0.2f,0.3f,0.f, 0.f,0.0f,0.f,
		0.2f,0.3f,0.f, 0.f,0.0f,0.f,
		0.2f,0.3f,0.f, 0.2f,-0.3f,0.f,

		0.8f,0.3f,0.f, 0.8f,-0.3f,0.f,
		0.8f,-0.3f,0.f, 0.5f,-0.3f,0.f,
		0.5f,-0.3f,0.f, 0.5f,0.f,0.f
	};// CPU memory에 있음 -> GPU memory로 옮겨야 함 (VBO필요)

	glGenBuffers( 1, &m_VBOName ); //just generate ID
	glBindBuffer( GL_ARRAY_BUFFER, m_VBOName); //용도 부여
	glBufferData( GL_ARRAY_BUFFER, sizeof( vertices ), vertices, GL_STATIC_DRAW ); //CPU -> GPU 복사

	//Create Many Particles
	CreateParticle( 10000 );
}

void Renderer::CreateParticle( int count )
{
	float* particleVertices = new float[count * 15 * 3 * 2];	//2 triangles 3 vertices 15 floats
	int floatCount = count * 15 * 3 * 2;
	int vertexCount = count * 3 * 2;

	int index = 0;
	float particleSize = 0.01f;

	for ( int i = 0; i < count; ++i )
	{
		//quad

		float randomValueX = 0.f;
		float randomValueY = 0.f;
		float randomValueZ = 0.f;
		float randomValueVX = 0.f;
		float randomValueVY = 0.f;
		float randomValueVZ = 0.f;
		float randomStartTime = 0.f;
		float randomLifeTime = 0.f;
		float randomPeriod = 0.f;
		float randomAmp = 0.f;
		float randomValue = 0.f;
		float randomR, randomG, randomB, randomA = 0.f;

		randomValueX = 0.f;// ( rand() / ( float )RAND_MAX - 0.5 ) * 2.f;
		randomValueY = 0.f;// ( rand() / ( float )RAND_MAX - 0.5 ) * 2.f;
		randomValueZ = 0.f;

		randomValueVX = 0.f;//( rand() / ( float )RAND_MAX - 0.5 ) * 0.5f;
		randomValueVY = 0.f;//( rand() / ( float )RAND_MAX - 0.5 ) * 0.5f;
		randomValueVZ = 0.f;

		randomStartTime = ( rand() / ( float )RAND_MAX ) * 6.f;
		randomLifeTime = 5.f;// ( rand() / ( float )RAND_MAX ) * 3.f;

		randomPeriod = ( rand() / ( float )RAND_MAX ) * 3.f;
		randomAmp = ( rand() / ( float )RAND_MAX ) * 0.f;

		randomValue = ( rand() / ( float )RAND_MAX ) * 1.f;

		randomR = ( rand() / ( float )RAND_MAX );
		randomG = ( rand() / ( float )RAND_MAX );
		randomB = ( rand() / ( float )RAND_MAX );
		randomA = ( rand() / ( float )RAND_MAX );

		//v0
		particleVertices[index] = -particleSize / 2.f + randomValueX;
		++index;
		particleVertices[index] = -particleSize / 2.f + randomValueY;
		++index;
		particleVertices[index] = 0.f;
		++index;
		particleVertices[index] = randomValueVX;
		++index;
		particleVertices[index] = randomValueVY;
		++index;
		particleVertices[index] = randomValueVZ;
		++index;
		particleVertices[index] = randomStartTime;
		++index;
		particleVertices[index] = randomLifeTime;
		++index;
		particleVertices[index] = randomPeriod;
		++index;
		particleVertices[index] = randomAmp;
		++index;
		particleVertices[index] = randomValue;
		++index;
		particleVertices[index] = randomR;
		++index;
		particleVertices[index] = randomG;
		++index;
		particleVertices[index] = randomB;
		++index;
		particleVertices[index] = randomA;
		++index;
		//v1
		particleVertices[index] = particleSize / 2.f + randomValueX;
		++index;
		particleVertices[index] = -particleSize / 2.f + randomValueY;
		++index;
		particleVertices[index] = 0.f;
		++index;
		particleVertices[index] = randomValueVX;
		++index;
		particleVertices[index] = randomValueVY;
		++index;
		particleVertices[index] = randomValueVZ;
		++index;
		particleVertices[index] = randomStartTime;
		++index;
		particleVertices[index] = randomLifeTime;
		++index;
		particleVertices[index] = randomPeriod;
		++index;
		particleVertices[index] = randomAmp;
		++index;
		particleVertices[index] = randomValue;
		++index;
		particleVertices[index] = randomR;
		++index;
		particleVertices[index] = randomG;
		++index;
		particleVertices[index] = randomB;
		++index;
		particleVertices[index] = randomA;
		++index;
		//v2
		particleVertices[index] = particleSize / 2.f + randomValueX;
		++index;
		particleVertices[index] = particleSize / 2.f + randomValueY;
		++index;
		particleVertices[index] = 0.f;
		++index;
		particleVertices[index] = randomValueVX;
		++index;
		particleVertices[index] = randomValueVY;
		++index;
		particleVertices[index] = randomValueVZ;
		++index;
		particleVertices[index] = randomStartTime;
		++index;
		particleVertices[index] = randomLifeTime;
		++index;
		particleVertices[index] = randomPeriod;
		++index;
		particleVertices[index] = randomAmp;
		++index;
		particleVertices[index] = randomValue;
		++index;
		particleVertices[index] = randomR;
		++index;
		particleVertices[index] = randomG;
		++index;
		particleVertices[index] = randomB;
		++index;
		particleVertices[index] = randomA;
		++index;
		//v3
		particleVertices[index] = -particleSize / 2.f + randomValueX;
		++index;
		particleVertices[index] = -particleSize / 2.f + randomValueY;
		++index;
		particleVertices[index] = 0.f;
		++index;
		particleVertices[index] = randomValueVX;
		++index;
		particleVertices[index] = randomValueVY;
		++index;
		particleVertices[index] = randomValueVZ;
		++index;
		particleVertices[index] = randomStartTime;
		++index;
		particleVertices[index] = randomLifeTime;
		++index;
		particleVertices[index] = randomPeriod;
		++index;
		particleVertices[index] = randomAmp;
		++index;
		particleVertices[index] = randomValue;
		++index;
		particleVertices[index] = randomR;
		++index;
		particleVertices[index] = randomG;
		++index;
		particleVertices[index] = randomB;
		++index;
		particleVertices[index] = randomA;
		++index;
		//v4
		particleVertices[index] = particleSize / 2.f + randomValueX;
		++index;
		particleVertices[index] = particleSize / 2.f + randomValueY;
		++index;
		particleVertices[index] = 0.f;
		++index;
		particleVertices[index] = randomValueVX;
		++index;
		particleVertices[index] = randomValueVY;
		++index;
		particleVertices[index] = randomValueVZ;
		++index;
		particleVertices[index] = randomStartTime;
		++index;
		particleVertices[index] = randomLifeTime;
		++index;
		particleVertices[index] = randomPeriod;
		++index;
		particleVertices[index] = randomAmp;
		++index;
		particleVertices[index] = randomValue;
		++index;
		particleVertices[index] = randomR;
		++index;
		particleVertices[index] = randomG;
		++index;
		particleVertices[index] = randomB;
		++index;
		particleVertices[index] = randomA;
		++index;
		//v5
		particleVertices[index] = -particleSize / 2.f + randomValueX;
		++index;
		particleVertices[index] = particleSize / 2.f + randomValueY;
		++index;
		particleVertices[index] = 0.f;
		++index;
		particleVertices[index] = randomValueVX;
		++index;
		particleVertices[index] = randomValueVY;
		++index;
		particleVertices[index] = randomValueVZ;
		++index;
		particleVertices[index] = randomStartTime;
		++index;
		particleVertices[index] = randomLifeTime;
		++index;
		particleVertices[index] = randomPeriod;
		++index;
		particleVertices[index] = randomAmp;
		++index;
		particleVertices[index] = randomValue;
		++index;
		particleVertices[index] = randomR;
		++index;
		particleVertices[index] = randomG;
		++index;
		particleVertices[index] = randomB;
		++index;
		particleVertices[index] = randomA;
		++index;
	}

	glGenBuffers( 1, &m_VBOManyParticle );
	glBindBuffer( GL_ARRAY_BUFFER, m_VBOManyParticle );
	glBufferData( GL_ARRAY_BUFFER, sizeof( float ) * floatCount, particleVertices, GL_STATIC_DRAW );
	m_VBOManyParticleCount = vertexCount;
}

void Renderer::AddShader(GLuint ShaderProgram, const char* pShaderText, GLenum ShaderType)
{
	//쉐이더 오브젝트 생성
	GLuint ShaderObj = glCreateShader(ShaderType);

	if (ShaderObj == 0) {
		fprintf(stderr, "Error creating shader type %d\n", ShaderType);
	}

	const GLchar* p[1];
	p[0] = pShaderText;
	GLint Lengths[1];
	Lengths[0] = (GLint)strlen(pShaderText);
	//쉐이더 코드를 쉐이더 오브젝트에 할당
	glShaderSource(ShaderObj, 1, p, Lengths);

	//할당된 쉐이더 코드를 컴파일
	glCompileShader(ShaderObj);

	GLint success;
	// ShaderObj 가 성공적으로 컴파일 되었는지 확인
	glGetShaderiv(ShaderObj, GL_COMPILE_STATUS, &success);
	if (!success) {
		GLchar InfoLog[1024];

		//OpenGL 의 shader log 데이터를 가져옴
		glGetShaderInfoLog(ShaderObj, 1024, NULL, InfoLog);
		fprintf(stderr, "Error compiling shader type %d: '%s'\n", ShaderType, InfoLog);
		printf("%s \n", pShaderText);
	}

	// ShaderProgram 에 attach!!
	glAttachShader(ShaderProgram, ShaderObj);
}

bool Renderer::ReadFile(char* filename, std::string *target)
{
	std::ifstream file(filename);
	if (file.fail())
	{
		std::cout << filename << " file loading failed.. \n";
		file.close();
		return false;
	}
	std::string line;
	while (getline(file, line)) {
		target->append(line.c_str());
		target->append("\n");
	}
	return true;
}

GLuint Renderer::CompileShaders(char* filenameVS, char* filenameFS)
{
	GLuint ShaderProgram = glCreateProgram(); //빈 쉐이더 프로그램 생성

	if (ShaderProgram == 0) { //쉐이더 프로그램이 만들어졌는지 확인
		fprintf(stderr, "Error creating shader program\n");
	}

	std::string vs, fs;

	//shader.vs 가 vs 안으로 로딩됨
	if (!ReadFile(filenameVS, &vs)) {
		printf("Error compiling vertex shader\n");
		return -1;
	};

	//shader.fs 가 fs 안으로 로딩됨
	if (!ReadFile(filenameFS, &fs)) {
		printf("Error compiling fragment shader\n");
		return -1;
	};

	// ShaderProgram 에 vs.c_str() 버텍스 쉐이더를 컴파일한 결과를 attach함
	AddShader(ShaderProgram, vs.c_str(), GL_VERTEX_SHADER);

	// ShaderProgram 에 fs.c_str() 프레그먼트 쉐이더를 컴파일한 결과를 attach함
	AddShader(ShaderProgram, fs.c_str(), GL_FRAGMENT_SHADER);

	GLint Success = 0;
	GLchar ErrorLog[1024] = { 0 };

	//Attach 완료된 shaderProgram 을 링킹함
	glLinkProgram(ShaderProgram);

	//링크가 성공했는지 확인
	glGetProgramiv(ShaderProgram, GL_LINK_STATUS, &Success);

	if (Success == 0) {
		// shader program 로그를 받아옴
		glGetProgramInfoLog(ShaderProgram, sizeof(ErrorLog), NULL, ErrorLog);
		std::cout << filenameVS << ", " << filenameFS << " Error linking shader program\n" << ErrorLog;
		return -1;
	}

	glValidateProgram(ShaderProgram);
	glGetProgramiv(ShaderProgram, GL_VALIDATE_STATUS, &Success);
	if (!Success) {
		glGetProgramInfoLog(ShaderProgram, sizeof(ErrorLog), NULL, ErrorLog);
		std::cout << filenameVS << ", " << filenameFS << " Error validating shader program\n" << ErrorLog;
		return -1;
	}

	glUseProgram(ShaderProgram);
	std::cout << filenameVS << ", " << filenameFS << " Shader compiling is done.\n";

	return ShaderProgram;
}
unsigned char * Renderer::loadBMPRaw(const char * imagepath, unsigned int& outWidth, unsigned int& outHeight)
{
	std::cout << "Loading bmp file " << imagepath << " ... " << std::endl;
	outWidth = -1;
	outHeight = -1;
	// Data read from the header of the BMP file
	unsigned char header[54];
	unsigned int dataPos;
	unsigned int imageSize;
	// Actual RGB data
	unsigned char * data;

	// Open the file
	FILE * file = NULL;
	fopen_s(&file, imagepath, "rb");
	if (!file)
	{
		std::cout << "Image could not be opened, " << imagepath << " is missing. " << std::endl;
		return NULL;
	}

	if (fread(header, 1, 54, file) != 54)
	{
		std::cout << imagepath << " is not a correct BMP file. " << std::endl;
		return NULL;
	}

	if (header[0] != 'B' || header[1] != 'M')
	{
		std::cout << imagepath << " is not a correct BMP file. " << std::endl;
		return NULL;
	}

	if (*(int*)&(header[0x1E]) != 0)
	{
		std::cout << imagepath << " is not a correct BMP file. " << std::endl;
		return NULL;
	}

	if (*(int*)&(header[0x1C]) != 24)
	{
		std::cout << imagepath << " is not a correct BMP file. " << std::endl;
		return NULL;
	}

	dataPos = *(int*)&(header[0x0A]);
	imageSize = *(int*)&(header[0x22]);
	outWidth = *(int*)&(header[0x12]);
	outHeight = *(int*)&(header[0x16]);

	if (imageSize == 0)
		imageSize = outWidth * outHeight * 3;

	if (dataPos == 0)
		dataPos = 54;

	data = new unsigned char[imageSize];

	fread(data, 1, imageSize, file);

	fclose(file);

	std::cout << imagepath << " is succesfully loaded. " << std::endl;

	return data;
}

GLuint Renderer::CreatePngTexture(char * filePath)
{
	//Load Pngs: Load file and decode image.
	std::vector<unsigned char> image;
	unsigned width, height;
	unsigned error = lodepng::decode(image, width, height, filePath);
	if (error != 0)
	{
		lodepng_error_text(error);
		assert(error == 0);
		return -1;
	}

	GLuint temp;
	glGenTextures(1, &temp);

	glBindTexture(GL_TEXTURE_2D, temp);
	glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST);
	glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);
	glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA, width, height, 0, GL_RGBA, GL_UNSIGNED_BYTE, &image[0]);

	return temp;
}

GLuint Renderer::CreateBmpTexture(char * filePath)
{
	//Load Bmp: Load file and decode image.
	unsigned int width, height;
	unsigned char * bmp
		= loadBMPRaw(filePath, width, height);

	if (bmp == NULL)
	{
		std::cout << "Error while loading bmp file : " << filePath << std::endl;
		assert(bmp != NULL);
		return -1;
	}

	GLuint temp;
	glGenTextures(1, &temp);

	glBindTexture(GL_TEXTURE_2D, temp);
	glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST);
	glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);
	glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA, width, height, 0, GL_RGBA, GL_UNSIGNED_BYTE, bmp);

	return temp;
}

void Renderer::Test()
{
	//prepare step
	glUseProgram(m_SolidRectShader);
	int attribPosition = glGetAttribLocation(m_SolidRectShader, "a_Position");
	glEnableVertexAttribArray(attribPosition);
	glBindBuffer(GL_ARRAY_BUFFER, m_VBOTest); // m_VBOTest -> bind -> GL_ARRAY_BUFFER
	glVertexAttribPointer(attribPosition, 3, GL_FLOAT, GL_FALSE, sizeof(float) * 3, 0);

	//render
	glDrawArrays(GL_LINE_LOOP, 0, 3);

	glDisableVertexAttribArray(attribPosition);
}

void Renderer::DrawSign()
{
	//prepare step
	glUseProgram( m_SolidRectShader );
	int attribPosition = glGetAttribLocation( m_SolidRectShader, "a_Position" );
	glEnableVertexAttribArray( attribPosition );
	glBindBuffer( GL_ARRAY_BUFFER, m_VBOName ); // m_VBOTest -> bind -> GL_ARRAY_BUFFER
	glVertexAttribPointer( attribPosition, 3, GL_FLOAT, GL_FALSE, sizeof( float ) * 3, 0 );

	//render
	glDrawArrays( GL_LINES, 0, 20 );

	glDisableVertexAttribArray( attribPosition );
}

constexpr float PI = 3.141592;
float g_value = 0.f;

void Renderer::Lecture3(float elapsedTime)
{
	GLuint shader = m_SolidRectShader;
	glUseProgram( shader );//다른 USEPROGRAM이 불리기 전에 다 여기서 동작

	//vs input을 gl에 알려주는 작업 : vs imput(in vs) == c++ 에서 알려줘야 하는 정보
	//a_Position ( in vs ) : layout => 0
	GLuint posID = glGetAttribLocation( shader, "a_Position" );	//vec3 position
	glEnableVertexAttribArray( posID );
	glBindBuffer( GL_ARRAY_BUFFER, m_VBOSingleParticle );	//18개의float
	glVertexAttribPointer( posID, 3, GL_FLOAT, GL_FALSE, sizeof( float ) * 3, 0 );


	//GLuint angleID = glGetUniformLocation( shader, "u_Angle" );
	//glUniform1f( angleID, g_value );
	//g_value += 0.0001f;
	//if ( g_value > 2 * PI )
	//	g_value = 0.f;

	/*GLuint colID = glGetAttribLocation( shader, "a_Color" );
	glEnableVertexAttribArray( colID );
	glBindBuffer( GL_ARRAY_BUFFER, m_VBOTestColor );
	glVertexAttribPointer( colID, 4, GL_FLOAT, GL_FALSE, sizeof( float ) * 4, 0 );

	GLuint scaleID = glGetUniformLocation( shader, "u_Scale" );
	glUniform1f( scaleID, g_value );
	g_value += 0.0001f;
	if ( g_value > 1.f )
		g_value = 0.f;*/

	GLuint timeID = glGetUniformLocation( shader, "u_Time" );
	glUniform1f( timeID, m_TotalTimeLecture3);

	glDrawArrays( GL_TRIANGLES, 0, 6 );

	m_TotalTimeLecture3 += elapsedTime;
}

void Renderer::Lecture6( float elapsedTime )
{
	GLuint shader = m_SolidRectShader;
	glUseProgram( shader );//다른 USEPROGRAM이 불리기 전에 다 여기서 동작

	//vs input을 gl에 알려주는 작업 : vs imput(in vs) == c++ 에서 알려줘야 하는 정보
	//a_Position ( in vs ) : layout => 0
	GLuint posID = glGetAttribLocation( shader, "a_Position" );	//vec3 position
	glEnableVertexAttribArray( posID );
	glBindBuffer( GL_ARRAY_BUFFER, m_VBOManyParticle );	//18개의float
	glVertexAttribPointer( posID, 3, GL_FLOAT, GL_FALSE, sizeof( float ) * 7, ( GLvoid* )0 );
	GLuint velID = glGetAttribLocation( shader, "a_Vel" );
	glEnableVertexAttribArray( velID );
	glVertexAttribPointer( velID, 3, GL_FLOAT, GL_FALSE, sizeof( float ) * 7, ( GLvoid* )( sizeof( float ) * 3 ) );
	GLuint startTimeID = glGetAttribLocation( shader, "a_StartTime" );
	glEnableVertexAttribArray( startTimeID );
	glVertexAttribPointer( startTimeID, 1, GL_FLOAT, GL_FALSE, sizeof( float ) * 7, ( GLvoid* )( sizeof( float ) * 6 ) );


	GLuint timeID = glGetUniformLocation( shader, "u_Time" );
	glUniform1f( timeID, m_TotalTimeLecture3 );

	glDrawArrays( GL_TRIANGLES, 0, m_VBOManyParticleCount );

	m_TotalTimeLecture3 += elapsedTime;
}

void Renderer::Lecture8( float elapsedTime )	//particle 관련 study
{
	GLuint shader = m_SolidRectShader;
	glUseProgram( shader );//다른 USEPROGRAM이 불리기 전에 다 여기서 동작

	glEnable( GL_BLEND );
	glBlendFunc( GL_SRC_ALPHA, GL_ONE_MINUS_CONSTANT_ALPHA );

	//vs input을 gl에 알려주는 작업 : vs imput(in vs) == c++ 에서 알려줘야 하는 정보
	//a_Position ( in vs ) : layout => 0
	GLuint posID = glGetAttribLocation( shader, "a_Position" );	//vec3 position
	glEnableVertexAttribArray( posID );
	glBindBuffer( GL_ARRAY_BUFFER, m_VBOManyParticle );	//18개의float
	glVertexAttribPointer( posID, 3, GL_FLOAT, GL_FALSE, sizeof( float ) * 15, ( GLvoid* )0 );
	GLuint velID = glGetAttribLocation( shader, "a_Vel" );
	glEnableVertexAttribArray( velID );
	glVertexAttribPointer( velID, 3, GL_FLOAT, GL_FALSE, sizeof( float ) * 15, ( GLvoid* )( sizeof( float ) * 3 ) );
	GLuint startTimeID = glGetAttribLocation( shader, "a_StartTime" );
	glEnableVertexAttribArray( startTimeID );
	glVertexAttribPointer( startTimeID, 1, GL_FLOAT, GL_FALSE, sizeof( float ) * 15, ( GLvoid* )( sizeof( float ) * 6 ) );
	GLuint lifeTimeID = glGetAttribLocation( shader, "a_LifeTime" );
	glEnableVertexAttribArray( lifeTimeID );
	glVertexAttribPointer( lifeTimeID, 1, GL_FLOAT, GL_FALSE, sizeof( float ) * 15, ( GLvoid* )( sizeof( float ) * 7 ) );
	GLuint periodID = glGetAttribLocation( shader, "a_Period" );
	glEnableVertexAttribArray( periodID );
	glVertexAttribPointer( periodID, 1, GL_FLOAT, GL_FALSE, sizeof( float ) * 15, ( GLvoid* )( sizeof( float ) * 8 ) );
	GLuint ampID = glGetAttribLocation( shader, "a_Amp" );
	glEnableVertexAttribArray( ampID );
	glVertexAttribPointer( ampID, 1, GL_FLOAT, GL_FALSE, sizeof( float ) * 15, ( GLvoid* )( sizeof( float ) * 9 ) );
	GLuint valueID = glGetAttribLocation( shader, "a_Value" );
	glEnableVertexAttribArray( valueID );
	glVertexAttribPointer( valueID, 1, GL_FLOAT, GL_FALSE, sizeof( float ) * 15, ( GLvoid* )( sizeof( float ) * 10 ) );
	GLuint colorID = glGetAttribLocation( shader, "a_Color" );
	glEnableVertexAttribArray( colorID );
	glVertexAttribPointer( colorID, 4, GL_FLOAT, GL_FALSE, sizeof( float ) * 15, ( GLvoid* )( sizeof( float ) * 11 ) );


	GLuint timeID = glGetUniformLocation( shader, "u_Time" );
	glUniform1f( timeID, m_TotalTimeLecture3 );

	glDrawArrays( GL_TRIANGLES, 0, m_VBOManyParticleCount );

	m_TotalTimeLecture3 += elapsedTime;

	glDisable( GL_BLEND );
}

void Renderer::FSSandbox( float elapsedTime )
{
	m_TotalTimeFSSandbox += elapsedTime;

	GLuint shader = m_FSSandboxShader;
	glUseProgram( shader );//다른 USEPROGRAM이 불리기 전에 다 여기서 동작

	glEnable( GL_BLEND );
	glBlendFunc( GL_SRC_ALPHA, GL_ONE_MINUS_CONSTANT_ALPHA );

	GLuint time = glGetUniformLocation( shader, "u_Time" );
	glUniform1f( time, m_TotalTimeFSSandbox );

	glBindBuffer( GL_ARRAY_BUFFER, m_VBOFSSandbox );	//18개의float
	GLuint posID = glGetAttribLocation( shader, "a_Position" );	//vec3 position
	glEnableVertexAttribArray( posID );
	glVertexAttribPointer( posID, 3, GL_FLOAT, GL_FALSE, sizeof( float ) * 3, ( GLvoid* )0 );

	glDrawArrays( GL_TRIANGLES, 0, 6 );
	
	glDisable( GL_BLEND );
}